.PHONY: linux

linux:
	sudo cp -f bot.service /etc/systemd/system/bot.service
	sudo systemctl daemon-reload
	sudo systemctl enable bot.service
	sudo systemctl start bot.service
	sudo systemctl status bot.service
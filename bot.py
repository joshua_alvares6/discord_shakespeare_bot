#!/usr/bin/python3

import os
import numpy as np
import pandas as pd
import discord
from dotenv import load_dotenv
from random import randint

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
GUILD = os.getenv('DISCORD_GUILD1')

client = discord.Client()



class MyClient(discord.Client):

    def __init__(self):
        discord.Client.__init__(self)
        self.DATA = pd.read_csv('/usr/local/src/discord_shakespeare_bot/data.csv')
        #print("imported csv as: \n")
        #print(self.DATA)

    async def on_ready(self):
        #for guild in client.guilds:
         #   if guild.name == GUILD:
          #      break
        print(
            f'{client.user} has connected to the following guild:\n'
            f'{client.guilds[len(client.guilds)-1].name}(id: {client.guilds[len(client.guilds)-1].id})'
        )


    async def on_message(self, message):
        if message.author == client.user:
            return
        insulted = False
        if len(message.mentions) > 0:
            for member in message.mentions:
                if member == client.user:
                    await message.channel.send("How dare thou treat the great {name} with such disgusting disrespect!".format(name=client.user.name))
                    insulted = True
                    break
        if message.content.startswith('!insult ') or message.content.startswith('!INSULT ') or insulted == True:
            
            response = "Thou {one} {two} {three}!".format(one=self.DATA.at[randint(0, 59), 'Column 1'], two=self.DATA.at[randint(0, 59), 'Column 2'], three=self.DATA.at[randint(0, 59), 'Column 3'])
            if '!INSULT' in message.content:
                response = response.upper()
            if 'me' in message.content.lower():
                await message.channel.send(response)
            elif insulted == True:
                await message.channel.send("<@{id}>! {mes}".format(id=message.author.id, mes=response))
            elif len(message.mentions) > 0:
                for member in message.mentions:
                    #nickname = "@{name}".format(name=(str(member.nick)))
                    #print("found user: {name}".format(name=nickname))
                    response = "<@{id}>!!! ".format(id=member.id) + response
                await message.channel.send(response)
            else:
                await message.channel.send("Please inform thy loyal servant whom is undignified.")
    
client = MyClient()
client.run(TOKEN)